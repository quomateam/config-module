<?php

use yii\db\Migration;

/**
 * Class m180801_164012_dropdown_class
 */
class m150828_204135_dropdown_class extends Migration
{
    
    public function init() {
        $this->db = 'db_config';
        parent::init();
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('item', 'model', 'varchar(255)');
        $this->addColumn('item', 'model_name_attr', 'varchar(255)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('item', 'model');
        $this->dropColumn('item', 'model_name_attr');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180801_164012_dropdown_class cannot be reverted.\n";

        return false;
    }
    */
}
