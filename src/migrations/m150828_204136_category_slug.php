<?php

use yii\db\Migration;

/**
 * Class m150828_204136_category_slug
 */
class m150828_204136_category_slug extends Migration
{

    public function init()
    {
        $this->db = 'db_config';
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('category', 'name', 'varchar(100)');
        $this->addColumn('category', 'slug', 'varchar(110)');

        $categories = \quoma\modules\config\models\Category::find()->all();

        foreach($categories as $category){
            $category->updateAttributes(['slug' => \yii\helpers\Inflector::slug($category->name)]);
        }

        $this->createIndex('slug_category_index', 'category', 'slug', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('category', 'slug');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181017_231604_category_slug cannot be reverted.\n";

        return false;
    }
    */
}
