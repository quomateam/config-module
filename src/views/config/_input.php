<?php
use yii\helpers\Html;
use quoma\modules\config\ConfigModule;
?>

<div class="form-group<?php if($model->hasErrors()) echo ' has-error' ?>">
    <?php 
    switch ($model->type){
    //De acuerdo al tipo de dato:
    case 'checkbox':
        
        echo Html::checkbox($model->attr, $model->value, ['uncheck' => 0, 'label' => $model->label]);
        break;
    
    case 'textarea':
        
        echo Html::label($model->label, $model->attr, ['class' => 'control-label']);
        echo Html::textarea($model->attr, $model->value, ['class' => 'form-control', 'rows' => 5]);
        break;
    
    case 'dropdownModel':
        
        $items = $model->getModelDropdownItems();
        
        echo Html::label($model->label, $model->attr, ['class' => 'control-label']);
        echo Html::dropDownList($model->attr, $model->value, $items, ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select...')]);
        break;

    case 'media':    
    case 'image':
    case 'images':
        ?>
        <div class="row">
            <div class="col-md-12">
                <h3><?php echo Yii::t('app', 'Images' ) ?></h3>
                <?php
                $mediaTypes = isset(Yii::$app->params['config']['mediaTypes']) ? Yii::$app->params['config']['mediaTypes'] : ['Image']; 

                echo \quoma\media\components\widgets\Buttons::widget([
                    'types' => $mediaTypes,
                    'media' => $model->media,
                    'previewOptions' => [
                        'update' => true,
                        'containerOptions' => ['class' => 'col-xs-6 col-md-3'],
                        'buttonsTemplate' => '{delete}',
                        'showControls' => false,
                        'inputName' => $model->attr
                    ]
                ]);
                ?>
            </div>
        </div>

        <?php
        break;

    default:
        
        echo Html::label($model->label, $model->attr, ['class' => 'control-label']);
        echo Html::input($model->type, $model->attr, $model->value, ['class' => 'form-control']);
    } ?>
    
    <?php if($model->description): ?>
    
        <div class="help-block"><?= $model->description ?></div>
    
    <?php endif; ?>
    
    <?= Html::error($model, 'value', ['class' => 'help-block']); ?>
</div>