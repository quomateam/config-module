<?php

namespace quoma\modules\config\models;

use quoma\media\behaviors\MediaBehavior;
use Yii;
use quoma\modules\config\ConfigModule;

/**
 * This is the model class for table "config".
 *
 * @property integer $config_id
 * @property integer $item_id
 * @property string $value
 *
 * @property Item $item
 */
class Config extends \quoma\core\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'config';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_config');
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        if (empty($this->item)) {
            return [];
        }

        $rules = [];
        foreach ($this->item->rules as $rule) {
            $rules[] = $rule->getLine();
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'config_id' => ConfigModule::t('Config ID'),
            'item_id' => ConfigModule::t('Item ID'),
            'value' => ConfigModule::t('Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem() {
        return $this->hasOne(Item::className(), ['item_id' => 'item_id']);
    }

    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable() {
        return true;
    }

    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: Item.
     */
    protected function unlinkWeakRelations() {
        
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            if ($this->getDeletable()) {
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $attr
     * @param bool $silent
     * @param int $cache
     * @return array|bool|Config|\yii\db\ActiveRecord|\yii\db\ActiveRecord[]|null
     * @throws \yii\web\HttpException
     */
    public static function getConfig($attr, $silent = false, $cache = 1) {

        $item = Item::find()->where(['attr' => $attr])->cache($cache)->one();
        if($item === null && $silent === false){
            throw new \yii\web\HttpException(404, 'Configuration item not found: ' . $attr);
        }elseif($item === null){
            return false;
        }
        
        if ($item->multiple) {
            $config = self::find()->where(['item_id' => $item->item_id])->cache(1)->all();
        } else {
            $config = self::find()->where(['item_id' => $item->item_id])->cache(1)->one();
        }

        if($item->isMediaType){
            return $item;
        }

        if (empty($config)) {
            $config = new self;
            $config->item_id = $item->item_id;
            $config->value = $item->default;
            $config->save();

            if ($item->multiple) {
                $config = [$config];
            }
        }

        return $config;
    }

    /**
     * @param $attr
     * @param bool $required
     * @return array|string
     * @throws \yii\web\HttpException
     */
    public static function getValue($attr, $required = false) {

        $config = self::getConfig($attr);

        if (is_array($config)) {
            return \yii\helpers\ArrayHelper::map($config, 'config_id', 'value');
        }

        if($required === true && ($config->value === null || $config->value === '')){
            throw new \yii\web\HttpException(500, Yii::t('app', '"{label}" ({attr}) must be configured.', ['attr' => $config->item->attr, 'label' => $config->item->label]));
        }

        if($config instanceof Item && $config->isMediaType){
            $media = $config->media;
            if($media && $config->type == 'image'){
                return $media[0];
            }
            return $media;
        }

        return $config->value;
    }

    /**
     * Mantenido por compatibilidad
     *
     * @param $attr
     * @param $value
     * @param bool $validate
     * @return array|Config|\yii\db\ActiveRecord|null
     * @throws \yii\web\HttpException
     */
    public static function setValue($attr, $value, $validate = true) {

        Item::setValue($attr, $value, $validate);

    }

    public function getAttr() {
        return $this->item->attr;
    }

    public function getLabel() {
        return $this->item->label;
    }

    public function getType() {
        return $this->item->type;
    }

    public function getDescription() {
        return $this->item->description;
    }
    
    /**
     * Devuelve el valor para el atributo solicitado. Si no existe y se proveen
     * datos para crearlo, intenta crearlo.
     * 
     * Para ver como inicializar los atributos:
     * https://quomasoftware.atlassian.net/wiki/spaces/BASEWEB/pages/32833538/Theme-Website+Config
     * 
     * @param string $attr
     * @param array $categoryOptions
     * @param array $configOptions
     * @return type
     */
    public static function get(string $attr, array $configOptions = [], array $categoryOptions = [])
    {
        $config = static::getConfig($attr, true);
        
        if($config === false){ 

            if(empty($configOptions)){
                throw new \yii\web\HttpException(500, 'Config item not found and initilization data not provided. ('.$attr.')');
            }

            $configOptions['attr'] = $attr;
            
            $transaction = Yii::$app->db->beginTransaction();
            try {
                //Category
                static::initializeCategory($categoryOptions);
                //Item
                static::initializeItem($configOptions);
                
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

        }
        
        return static::getValue($attr);

    }
    
    /**
     * Inicializa una categoria
     * @param array $category
     * @return boolean
     */
    protected static function initializeCategory($category)
    {
        //Validamos datos
        static::parseCategory($category);

        if(!Category::find()->where(['slug' => $category['slug']])->exists()){
            $newCategory = new Category();
            $newCategory->load($category,'');
            $newCategory->status = 'enabled';

            if($newCategory->save()){
                $newCategory->updateAttributes(['slug' => $category['slug']]);
                return true;
            }
            
        }
        
        return false;
    }
    
    /**
     * Inicializa las categorias
     * @param array $categories
     * @return int
     */
    protected static function initializeItem($item)
    {
        //Validamos datos
        static::parseItem($item);

        $category = Category::find()->where(['slug' => $item['category']])->cache(false)->one();

        if($category === null){
            throw new \yii\web\HttpException(500, 'Category "'.$item['category'].'" not found in item '.$item['attr'].'. Please, check config-init file.');
        }

        if(!Item::find()->where(['attr' => $item['attr'], 'category_id' => $category->category_id])->exists()){

            //El valor debe ser convertido a string para almacenar
            $item['default'] = (string)$item['default'];

            $newItem = new Item();
            $newItem->load($item,'');

            $newItem->category_id = $category->category_id;

            if($newItem->save()){
                //Reglas de validacion
                if(isset($item['rules']) && is_array($item['rules'])){
                    static::initializeRules($newItem, $item['rules']);
                }
            }else{
                throw new \yii\web\HttpException(500, Yii::t('app', 'Config item could not be inicialized. Errors: {errors}', ['errors' => $newItem->getErrorsAsString()]));
            }
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Inicializa las reglas de validacion para un item
     * @param array $categories
     * @return int
     */
    protected static function initializeRules($item, $rules)
    {
        $count = 0;
        
        foreach($rules as $rule){
            
            //Validamos datos
            static::parseRules($rule);
            
            foreach($rules as $rule){
                $newRule = new Rule();
                $newRule->load($rule,'');
                $newRule->item_id = $item->item_id;
                if($newRule->save()){
                    $count++;
                }
            }
            
        }
        
        return $count;
    }
    
    /**
     * Valida las categorias
     * @param array $data
     * @return boolean
     * @throws \yii\web\HttpException
     */
    public static function parseCategory($data)
    {
        $check = static::parseArray($data, [
            //Req:
            'name',
            'slug'
        ], [
            //Valid:
            'name',
            'slug',
            'superadmin'
        ]);
        
    }
    
    
    /**
     * Valida las categorias
     * @param array $data
     * @return boolean
     * @throws \yii\web\HttpException
     */
    public static function parseItem($data)
    {
        $check = static::parseArray($data, [
            //Req:
            'label',
            'attr',
            'type',
            'category',
            'default'
        ], [
            //Valid:
            'label',
            'attr',
            'type',
            'default',
            'category',
            'superadmin',
            'description',
            'rules',
            'model',
            'model_name_attr'
        ]);

    }
    
    public function getModelDropdownItems()
    {
        return $this->item->getModelDropdownItems();
    }

    /**
     * Devuelve 1 si faltan datos requeridos; devuelve 2 si hay datos con nombre
     * incorrecto. Devuelve 0 si todo esta bien.
     * @param array $data
     * @param array $required
     * @param array $validKeys
     * @return int
     */
    protected static function parseArray($data, $required, $validKeys)
    {

        //Validamos datos requeridos
        if(count(array_intersect_key(array_flip($required), $data)) !== count($required)) {
            $required = implode(', ', array_diff($required, array_keys($data)));
            throw new \yii\web\HttpException(500, "Bad configuration. Atributes '$required' are required.");
        }

        //Validamos todos los keys
        foreach($data as $key => $value){
            if(!in_array($key, $validKeys)){
                throw new \yii\web\HttpException(500, "Bad configuration. Atribute $key is not valid.");
            }
        }

        return 0;
    }
}
